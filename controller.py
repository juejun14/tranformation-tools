# -*- coding: utf-8 -*-
"""
Created on Mon Feb 28 16:33:51 2022

@author: Juejun
"""
from view_class import Interface
from BWT_class import *
from huffman_class import *


class Controller:
    """
    Controller for construction
    The controller take input from interface to computing model and return the result to interface
    """
    def __init__(self):
        # Define the variable to different class
        self.view = Interface(self)
        self.BWT = BWT_algo()
        self.HUFF = HuffmanTree()
        # Define the variable for different program
        self.entry = ''
        self.result, self.code_dict = '', ''
        self.show_step, self.show_all, self.press_button = '', '', ''

    def start_view(self):
        """ Start the interface """
        self.view.create_menu()
        self.view.main()

    def press_button_program(self, button):
        """ Read the data and button to select the corresponding program"""
        try:
            self.view.result_zone.destroy()
            self.view.step_button.destroy()
        except:
            pass
        # When BWT transformation is chosen
        if button == 'BWT':
            # Reset the program before beginning.
            self.BWT = BWT_algo()
            self.show_step, self.show_all = '', ''
            # Get the input from interface
            self.entry = self.view.get_value(self.view.bwt_entry)
            # The input can't be empty
            if self.entry == '':
                self.view.show_error()
            # Define the program to encode or decode and check if input is valid
            elif self.view.prog == 11:
                check = self.view.is_DNA(self.entry)
                if check is False:
                    self.view.show_error()
                # Compute input if input is valid
                else:
                    self.compute_BWT()
            elif self.view.prog == 21:
                check = self.view.is_RNA(self.entry)
                if check is False:
                    self.view.show_error()
                else:
                    self.compute_BWT()
            elif self.view.prog == 31:
                self.compute_BWT()
            # Decode program is chosen
            else:
                self.result = self.BWT.BWT_Decode(self.entry)
            # Return false when result is invalid
            if self.result is False:
                self.view.show_error()
            else:
                self.view.show_result(self.view.BWT_result)
        # When Huffman coding transformation is chosen
        elif button == 'HUFF':
            # Reset the program from beginning.
            self.HUFF = HuffmanTree()
            self.show_step, self.show_all = '', ''
            self.entry = self.view.get_value(self.view.huff_entry)
            # Control the entry should be valid.
            if self.entry == '':
                self.view.show_error()
            elif self.view.prog == 11:
                check = self.view.is_DNA(self.entry)
                if check is False:
                    self.view.show_error()
                else:
                    self.compute_HUFF()
            elif self.view.prog == 21:
                check = self.view.is_RNA(self.entry)
                if check is False:
                    self.view.show_error()
                else:
                    self.compute_HUFF()
            elif self.view.prog == 31:
                self.compute_HUFF()
            # When decode program is chosen
            else:
                try:
                    # Try to convert the first line of data as a code dictionary to decode
                    deco_dico = self.view.code_entry.get()
                    deco_dico = eval(deco_dico)
                    binary_sequence = self.HUFF.octet_to_binary(self.entry)
                    self.result = self.HUFF.decode(binary_sequence, deco_dico)
                except:
                    # If the first line is not a dictionary type
                    self.view.show_error()
            # Show error message if the data can't be converted
            if self.result is False:
                self.view.show_error()
            else:
                self.view.show_result(self.view.Huff_result)

    def compute_BWT(self):
        """ Call the computing model to encode data """
        try:
            self.result = self.BWT.BWT_Encode(self.entry)
        except:
            self.view.show_error()

    def compute_HUFF(self):
        """ Call the computing model to encode data """
        try:
            # Start the transformation program
            self.entry = self.HUFF.purify(self.entry)
            freq = self.HUFF.frequency(self.entry)
            self.HUFF.generate_tree(freq)
            self.code_dict = self.HUFF.get_code()
            self.HUFF.get_code_sequence(self.entry)
            self.result = self.HUFF.binary_to_octet()
        except:
            # Show error if the program can't be finished correctly
            self.view.show_error()

    def press_next(self):
        """ Get the click event to show every transformation step """
        # Define the press event variable
        self.press_button = 'next'
        # Call iterator from BWT class
        if self.view.open_window == "BWT":
            # Show each step by line
            self.show_step += str(next(self.BWT, "Finished!")) + '\n'
            self.view.show_result(self.view.BWT_result)
            # Show finished info when the program is finished
            if "Finished!" in self.show_step:
                self.view.show_info()
                return False
        # Call iterator from Huffman coding class
        elif self.view.open_window == "HUFF":
            # Show each step by line
            self.show_step += str(next(self.HUFF, "Finished!")) + '\n'
            self.view.show_result(self.view.Huff_result)
            # Show finished info when the program is finished
            if "Finished!" in self.show_step:
                self.view.show_info()
                return False

    def press_show_all(self):
        """ Get the click event to show all transformation step """
        # Define the press event variable and reset the result content
        self.press_button = 'show all'
        self.show_all = ''
        if self.view.open_window == "BWT":
            # Count the number of elements of the iterator and return corresponding content
            for time in range(len(self.BWT.step)):
                line = self.BWT.step[time]
                list_to_line = ''
                # If the elements of one step is stored as a list, return the list content by line
                while type(line) == list:
                    for i in line:
                        list_to_line += str(i) + '\n'
                    line = list_to_line
                    break
                self.show_all += str(line) + '\n'
            self.view.show_result(self.view.BWT_result)

        elif self.view.open_window == "HUFF":
            # Return corresponding content and avoid if 'next' button is already called.
            for time in range(len(self.HUFF.step)):
                line = self.HUFF.step[time]
                list_to_line = ''
                # If the elements of one step is stored as a list, return the list content by line
                while type(line) == list:
                    for i in line:
                        list_to_line += str(i) + '\n'
                    line = list_to_line
                    break
                self.show_all += str(line) + '\n'
            self.view.show_result(self.view.Huff_result)

    def press_save(self):
        """ Function to call corresponding program to save the transformation result """
        # Get the file path
        self.view.save_as()
        # If the file path is empty
        if not self.view.file_name:
            return False
        # If the file path is not empty, call corresponding program
        else:
            if self.view.open_window == "BWT":
                self.BWT.save_result(self.view.file_name, self.result)
            elif self.view.open_window == "HUFF":
                self.HUFF.save_result(self.view.file_name, self.result, self.code_dict)

    def press_import(self):
        """ Function to import data to compute """
        self.view.import_file()
        # If the file path is empty
        if not self.view.file_name:
            return False
        # If the file path is not empty, call corresponding program
        else:
            # When the BWT program is chosen.
            if self.view.open_window == "BWT":
                # Return all lines of data by one time
                line = import_bwt_file(self.view.file_name)
                # Set the content to input
                self.view.bwt_entry.set(line)
                # Show info
                self.view.show_success()
            # When the Huffman coding program is chosen.
            elif self.view.open_window == "HUFF":
                # Use two variable to store the code dictionary and the data
                line, code = import_huff_file(self.view.file_name)
                # Check if the first line is a dictionary type
                if (type(code)) == dict:
                    # Detect and set the type of data and program
                    self.view.p.set(2)
                    self.view.type_content.pack_forget()
                    # Show the dictionary widget while import
                    self.view.code_dict.pack()
                    # Fill the entry widget with the import content
                    self.view.huff_entry.set(line)
                    self.view.code_entry.set(code)
                    # Start computing
                    binary_sequence = self.HUFF.octet_to_binary(line)
                    self.result = self.HUFF.decode(binary_sequence, code)
                    self.view.show_result(self.view.Huff_result)
                    self.view.show_success()

                # When the first line is not dictionary type, considerate the file is to be encoded
                elif (type(code)) != dict:
                    self.view.huff_entry.set(line)


if __name__ == "__main__":
    CONTROLLER = Controller()
    CONTROLLER.start_view()
