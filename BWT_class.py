# -*- coding: utf-8 -*-
"""
Created on Tue Feb 22 22:36:40 2022

@author: Juejun
"""


class BWT_algo:
    """
    Class for the Burrows Wheeler Transform, used to transfer DNA or other sequences.
    """
    def __init__(self):
        # Define the variable to save all step to print
        self.step = []
        self.counter = 0
        self.prog_type = ''
        self.result = ''

    def BWT_Encode(self, seq):
        """
        Function to encode the data.
        """
        # Check if the input is valid.
        if '$' in seq:
            print("The sequence is already encoded possibly, please use decode program.")
            return False

        elif '$' not in seq:
            # Define the type of program.
            self.prog_type = "encode"

            print("The original sequence is :", seq)
            # Uniform the sequence format
            seq = seq.upper().replace(' ', '').replace('\n', '') + "$"
            self.step.append("The first step: \n")
            self.step.append(seq)

            seq_length = list(range(len(seq)))
            BWT_list = []
            # Repeated n times when n = len(sequence)
            for times in seq_length:    
                for i in range(1):   
                    seq = list(seq)
                    # Insert the last element to first place
                    seq.insert(0, seq.pop())
                    seq = ''.join(seq)
                BWT_list.append(seq)    
                self.step.append(seq)
            # Sort the elements of a given iterable in a specific order
            self.BWT_list = sorted(BWT_list)
            # Save the step to list
            self.step.append("\nThe second step: \n")
            for i in self.BWT_list:
                self.step.append(i)
            # Define the variable tu store the final result, l is the last letter of each line
            last_row = ''
            # Add letter to generate a string
            for i in range(len(self.BWT_list)):
                last_row += self.BWT_list[i][-1]
            print("The encode sequence is :" + last_row)
            self.result = last_row
            # Add step to list
            self.step.append("\nThe third step: Take the last character from each line \n")
            self.step.append(str("The encode sequence is :" + last_row))
            return self.result
        else:
            print("here")
            print("The input is invalid, please re-enter your sequence.")
            return False

    def BWT_Decode(self, chain):
        """
        Create a list with n elements.(n = length of sequence)
        Add separately the character into element for each time 
        and sort list by lexicographic order.
        For the last time, stop the loop 
        and select the sequence which is ended by the "$".
        Remove the "$" and return the original DNA sequence
        """
        # Confirm the data is a BWT encoding file.
        if chain.count('$') == 1:
            # Define the type of program.
            self.prog_type = "decode"
            # Store the original string to list
            self.step.append(chain)
            # Convert each character of string to list and store to step list
            chain = list(chain)
            self.step.append(chain)
            # Sort the list by order and store into another variable
            matrix = sorted(chain)
            self.step.append(matrix)
            # Create a list to set the repeated times of loop
            seq_length = list(range(len(chain)-2))
            for times in seq_length:
                # Add the character at the beginning of string and sort the list
                for i in range(len(matrix)):
                    matrix[i] = chain[i] + matrix[i]
                matrix = sorted(matrix)
                self.step.append(matrix)
            # Add the character at the beginning of string for the last time and don't sort the list
            for i in range(len(matrix)):
                matrix[i] = chain[i] + matrix[i]
            # Store each step to list
            self.step.append(matrix)
    
            sequence = ''
            for i in matrix:
                # Find the string ended by '$'
                if i.find('$') == len(i) - 1:
                    sequence = i
            self.step.append(sequence)
            # Cut the '$' of string
            sequence = sequence.strip('$')
            self.step.append(("The decode sequence is :" + sequence))
            # Store the final result
            self.result = sequence
            # Return false and show error message if not result
            if self.result == '':
                print("The input is invalid, please re-enter your sequence.")
                return False
            else:
                return self.result
        else:
            print("The input is invalid, please re-enter your sequence.")
            return False

    def save_result(self, file, result):
        """ Function to save the result to file """
        # Check if the result is empty
        if result == '':
            print("There is not result to save.")
        elif result != '':
            print("Saved")
            file = open(file.name, "w", encoding='UTF-8')
            file.write(result)
            file.close()
    
    def __iter__(self):
        return self 
    
    def __next__(self):
        """ Function to read the iterator """
        if self.prog_type == "encode":
            if self.counter == len(self.step):
                raise StopIteration
            step = self.step[self.counter]
            # Update the counter to show the next element
            self.counter += 1
            return step
        elif self.prog_type == "decode":
            if self.counter == len(self.step):
                raise StopIteration
            step = self.step[self.counter]
            self.counter += 1
            # Show the list element by row
            while type(step) == list:
                self.by_row = ''
                for i in step:
                    self.by_row += i + '\n'
                return self.by_row
            return step


def import_bwt_file(file):
    """ Function to import the data from the file path"""
    with open(file, 'r', encoding='UTF-8') as f:
        line = f.read()
        print(line)
    return line


# Example to test class
if __name__ == "__main__":
    seq = 'T$ACG'
    test = BWT_algo()
    test.BWT_Decode(seq)

    for i in range(10):
        print(next(test, "Finished"))

