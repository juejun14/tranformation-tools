# -*- coding: utf-8 -*-
"""
Created on Sat Feb 26 21:24:49 2022

@author: Juejun
"""
from node_class import Node


class HuffmanTree():
    """
    Class to the huffman coding.
    """
    def __init__(self):
        # Define variable for different function
        self.freq = {}
        self.Leaf = []
        self.root = self.Leaf
        self.result = ''
        # Define the max depth of the tree                        
        self.Buffer = []
        self.binary = {}
        # Define the original program is None
        self.program = 0
        # Define the variable to save all step to print
        self.step = []
        self.counter = 0

    def purify(self, sequence):
        """
        Function to uniform the sequence.
        """
        # Uniform sequence format
        sequence = sequence.upper().replace(' ', '').replace('\n', '')
        # Add the first step to list
        self.step.append(str("The DNA sequence is :" + sequence))
        return sequence

    def frequency(self, sequence):
        """
        Function to generate the nucleotide frequency dictionary.
        """
        # Define the frequency element as a dictionary
        freq = {}
        # Count each element of sequence and add element as the key, add frequency as value
        for element in sequence:
            freq[element] = sequence.count(element)
        # Add the next step to list
        self.step.append(str("The frequency of each nucleotide is : " + str(freq)))
        return freq

    def generate_tree(self, freq):
        """
        Generate the binary tree
        """
        # Create node and named node by dictionary
        self.Leaf = [Node(key, value) for key, value in freq.items()]
        # The maximal depth of tree is the length of node list
        self.Buffer = list(range(len(self.Leaf)))
        # Create the tree
        while len(self.Leaf) != 1:
            # Decreasing order
            self.Leaf.sort(key=lambda node: node.value, reverse=True)
            # Create new node which is the sum of two nodes 
            n = Node(value=(self.Leaf[-1].value + self.Leaf[-2].value))
            # Deleted the 2 minimal node from self.Leaf and set as the child node
            n.left = self.Leaf.pop(-1)
            n.right = self.Leaf.pop(-1)
            # Add new node into self.Leaf
            self.Leaf.append(n)
        # Define the first node as root
        self.root = self.Leaf[0]
        # Display the tree
        self.tree = self.root.display()
        # Add huffman tree to list
        self.step.append(self.tree)

    def Huff_generate(self, tree, length):
        """
        Generate the Huffman tree to get the huffman code
        """
        node = tree
        binary_value = ''
        # Replace the depth value by the binary code for each node
        if not node:
            return
        elif node.name:
            for i in range(length):
                binary_value += str(self.Buffer[i])
            # Save each result to a dictionary
            self.binary[node.name] = binary_value
        # If left node, the node value is 0
        self.Buffer[length] = 0
        self.Huff_generate(node.left, length + 1)
        # If right node, the node value is 1
        self.Buffer[length] = 1
        self.Huff_generate(node.right, length + 1)

    def get_code(self):
        """ Return the huffman code for each node of tree """
        self.Huff_generate(self.root, 0)
        # Add the nucleotide code to list
        self.step.append("The nucleotide code is : ")
        self.step.append(str(self.binary))
        return self.binary

    def get_code_sequence(self, string):
        """ Return the binary sequence """
        for key, value in self.binary.items():
            for i in string:
                if i == key:
                    string = string.replace(i, value)
        self.string = string
        # Add the binary sequence to list
        self.step.append("The sequence code is : ")
        self.step.append(str(self.string))
        return self.string
    
    def binary_to_octet(self):
        """
        The binary sequence will be transformed to 8 bytes,
        a '11111111' tail will be added at the end of the sequence to avoid the transformation error.
        """
        # Set split interval as 8 characters
        n = 8
        # Define the list to store the value
        decimal = []
        octet = []
        # Add a '11111111' tail at the end of string
        self.string = self.string +'11111111'
        # Split string and store each element to list
        bit_list = [self.string[i:i+n] for i in range(0, len(self.string), n)]
        # Add result to step list
        self.step.append("Separate the sequence to 8 bytes and add a tail: ")
        self.step.append(bit_list)
        # Convert 8 bytes to decimal
        for i in range(len(bit_list)):
            decimal.append(int(bit_list[i], 2))
        self.step.append("Convert 8 byte sequence to decimal : ")
        self.step.append(str(decimal))
        # Convert decimal to octet
        for i in range(len(decimal)):
            octet.append(chr(decimal[i]))
            # Connect all the element to a sequence
            self.result = "".join(octet)
        self.program = 1

        self.step.append("The transform result is : " + self.result)
        return self.result

    def octet_to_binary(self, result):
        """
        Transform octet to binary
        Firstly, the octet sequence is transform into decimal.
        Secondly, transform the decimal sequence to 8 bit binary
        and fill 0 if the length of element is less than 8 bit.
        Merge the list to obtain original binary sequence.

        result : octet sequence
        binary_seq : binary sequence from octet
        """
        # Define the program will be a decode program
        self.program = 2
        # Add result input to the step list
        self.step.append(result)
        result = list(result)

        l1, l2, l3 = [], [], []
        # Return the unicode code from a given character and add result to the list 'l1'
        for i in range(len(result)):
            l1.append(ord(result[i]))
        self.step.append("Convert to the unicode :")
        self.step.append(l1)
        # Return the unicode to binary format and add to the list 'l2'
        for i in range(len(l1)):
            l2.append(format(l1[i], 'b'))
        self.step.append("Convert the unicode to binary format :")
        self.step.append(l2)
        # Complete the element to 8 bytes with '0' if it's necessary.
        for i in range(len(l2)-1):
            l3.append(l2[i].zfill(8))
        l3.append(l2[-1])
        self.step.append("Complete the element to 8 bytes with '0' if it's necessary :")
        self.step.append(l3)
        binary_seq = ''.join(l3)
        self.step.append("The binary sequence :")
        self.step.append(binary_seq)
        # deleted the separator at the end of sequence
        binary_seq = binary_seq[:-8]
        # Cut the supplementary tail and return the original binary sequence
        self.step.append("Cut the supplementary tail : '11111111'")
        self.step.append("The binary sequence is : " + binary_seq)
        return binary_seq

    def decode(self, binary_seq, code_dict):
        """
        Compare the binary sequence with nucleotide code,
        if found, replace the binary string by the corresponding nucleotide.
        repeat the comparison until there isn't the binary code.
        """
        self.step.append("The binary dictionary is :" + str(code_dict))
        # Get the code dictionary and save key and value to different lists
        reference = []
        nucleotide = []
        for k, v in code_dict.items():
            reference.append(v)
            nucleotide.append(k)
        result = ''
        # Replace the binary sequence by the corresponding keys
        while len(binary_seq) > 0:
            for i in range(len(reference)):
                if self.cmp_ok(binary_seq, reference[i]) is True:
                    h = len(reference[i])
                    binary_seq = binary_seq[h:]
                    result = result + nucleotide[i]
        self.deco_result = result
        self.step.append("The decode sequence is :" + result)
        return self.deco_result
     
    def cmp_ok(self, seq, code):
        """ Check if the huffman code is in binary sequence. """
        l = len(code)
        k = 0
        # Confirm the sequence is longer than the key
        if len(seq) < len(code):
            return False
        # Comparer the binary sequence with key, lengthen the string if their string is the same
        while k < l:
            if seq[k] == code[k]:
                k += 1
            else:
                return False
        return True

    def save_result(self, file, result, dico):
        """
        Save the result in a file, the binary dictionary will be saved.
        """
        # When the encoding program is chosen.
        if self.program == 1:
            if result == '':
                print("There is not result to save.")
            elif result != '':
                # Open the file and write down the content
                file = open(file.name, "w", encoding='UTF-8')
                file.write(str(dico)+"\n")
                file.write(result)
                file.close()
                print("Saved.")
        # When the decoding program is chosen.
        elif self.program == 2:
            if self.deco_result == '':
                print("There is not result to save.")
            elif self.deco_result != '':
                # Open the file and write down the content
                file = open(file.name, "w", encoding='UTF-8')
                file.write(self.deco_result)
                file.close()

    def __iter__(self):
        return self

    def __next__(self):
        if self.program == 1:
            if self.counter == len(self.step):
                raise StopIteration
            step = self.step[self.counter]
            self.counter += 1
            # Show the tree by line
            while type(step) == list:
                self.by_line = ''
                for i in step:
                    self.by_line += str(i) + '\n'
                return self.by_line
            return step
        elif self.program == 2:
            if self.counter == len(self.step):
                raise StopIteration
            step = self.step[self.counter]
            self.counter += 1
            return step


def import_huff_file(file):
    # Open the file and get the first line
    with open(file, 'r', encoding='UTF-8') as f:
        line_1 = f.readline()
        # Check if the first line of file can be converted to dictionary
        try:
            line_1 = eval(line_1)
        except:
            pass
        if type(line_1) == dict:
            print("The data is to decode.")
            # Read all data
            line = f.read()
            return line, line_1
        else:
            # When input data is not a dictionary, the data will be encoded.
            print("The data is to encode.")
            # Return all lines to a variable and uniform the data format.
            all_line = line_1 + f.read()
            tree = HuffmanTree()
            all_line = tree.purify(all_line)
            return all_line, line_1


# Example for encoding
if __name__ == "__main__":
    seq = 'ACGTACGTA'
    test = HuffmanTree()
    seq = test.purify(seq)
    freq = test.frequency(seq)
    test.generate_tree(freq)
    test.get_code()
    test.get_code_sequence(seq)
    test.binary_to_octet()

    for i in range(13):
        print(next(test, 'Finished'))
