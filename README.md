# Transformation tools

This is an interface to convert the data by different algorithm.

## Description

This is the program to compress or decompress the data.

You have two type of transformation program to choose: 
* Burrows-Wheeler Transform
* Huffman coding

The data computed will be converted into another series of characters to reduce the original file size.\
And you can also compute the encoding file to get the original file.

## Installation

You can download the scripts from Gitlab by command.
```bash
git clone git@gitlab.com:juejun14/tranformation-tools.git
cd tranformation-tools

# Start with the main file
python3 controller.py
```

## Usage

Example:
1. Choose the program from the homepage
2. Choose what kind of sequence you want to convert
3. Choose encode or decode program
4. Enter your data and click "Compute" button to get the convert result
5. Save the result from the button of menu bar if is necessary

And you also can choose the display mode from the menu bar to follow the transformation step.

## Author
Juejun CHEN
