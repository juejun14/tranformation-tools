# -*- coding: utf-8 -*-
"""
Created on Tue Mar  1 15:30:57 2022

@author: Juejun
"""


class Node:
    """
    Class to generate the node and print the binary tree.
    """
    def __init__(self, name=None, value=None):        
        """
        Class constructor
        """
        self.name = name
        self.value = value
        self.left = None
        self.right = None

    def uniform(self):
        """ Replace the 'None' for name of node """
        line = ''
        if self.name is None:
            line = 'Node: %s' % self.value
        elif self.name is not None:
            line =  '%s : %s' % (self.name, self.value)
        return line

    def display(self):
        """ print the binary tree """
        self.figure = []
        lines, *_ = self._print_tree()
        for line in lines:
            self.figure.append(line)
        # Save the tree figure as a list
        return self.figure

    def _print_tree(self):
        """
        Returns list of strings, width, height, 
        and horizontal coordinate of the root.
        """

        # If no child.
        if self.right is None and self.left is None:
            line = '%s : %s' % (self.name, self.value)
            width = len(line)
            height = 1
            middle = width // 2
            return [line], width, height, middle

        # If only left child.
        if self.right is None:
            lines, n, p, x = self.left._print_tree()
            s = '%s : %s' % (self.name, self.value)
            u = len(s)
            first_line = (x + 1) * ' ' + (n - x - 1) * '_' + s
            second_line = x * ' ' + '/' + (n - x - 1 + u) * ' '
            shifted_lines = [line + u * ' ' for line in lines]
            return [first_line, second_line] + shifted_lines, n + u, p + 2, n + u // 2

        # If only right child.
        if self.left is None:
            lines, n, p, x = self.right._print_tree()
            s = self.uniform()
            u = len(s)
            first_line = s + x * '_' + (n - x) * ' '
            second_line = (u + x) * ' ' + '\\' + (n - x - 1) * ' '
            shifted_lines = [u * ' ' + line for line in lines]
            return [first_line, second_line] + shifted_lines, n + u, p + 2, u // 2

        # If two children.
        left, n, p, x = self.left._print_tree()
        right, m, q, y = self.right._print_tree()
        s = self.uniform()
        u = len(s)
        # Print the line and insert the name and value of node in middle
        first_line = (x + 1) * ' ' + (n - x - 1) * '_' + s + y * '_' + (m - y) * ' '
        # Print the branche
        second_line = x * ' ' + '/' + (n - x - 1 + u + y) * ' ' + '\\' + (m - y - 1) * ' '
        if p < q:
            left += [n * ' '] * (q - p)
        elif q < p:
            right += [m * ' '] * (p - q)
        zipped_lines = zip(left, right)
        lines = [first_line, second_line] + [a + u * ' ' + b for a, b in zipped_lines]
        return lines, n + m + u, max(p, q) + 2, n + u // 2

    def __str__(self):
        return str(self.name) + "\n" + str(self.left) + ' ' + str(self.right)
