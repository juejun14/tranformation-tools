# -*- coding: utf-8 -*-
"""
Created on Mon Feb 28 16:33:51 2022

@author: Juejun
"""
from tkinter import Tk, ttk
from tkinter import Button, Entry, Label
from tkinter import StringVar, IntVar
from tkinter import messagebox, Radiobutton
from tkinter import Message, Menu
from tkinter import Frame, LabelFrame
from tkinter import scrolledtext
from tkinter import filedialog


class Interface:
    """
    Generate the interface
    """
    def __init__(self, controller):
        super().__init__()
        self.controller = controller
        self.window = Tk()
        # Define the program widget
        self.Home_frame, self.BWT_title, self.Huff_title = '', '', ''
        # Define variable for the open widget
        self.open_window = ''
        self.result_zone = ''
        # Define the program and the variable by default
        self.prog = 31
        self.s, self.p = IntVar(), IntVar()
        self.bwt_entry, self.huff_entry, self.code_entry = StringVar(), StringVar(), StringVar()

        # The variable to define the type of sequence and program
        self.s.set(3)
        self.p.set(1)
        # Define to show result directly by default
        self.check_mode = IntVar()
        self.check_mode.set(1)

    def create_menu(self):
        """
         Create the main menu for interface
        """
        global file_menu, mode_menu
        # Create the first menu
        menubar = Menu(self.window)
        file_menu = Menu(menubar, tearoff=0)
        file_menu.add_command(label="Homepage", command=lambda: self.Homepage(self.window))
        # Create the button and set it as disabled when the program is not chosen yet
        file_menu.add_command(label="Import file from...", state='disabled',
                              command=lambda: self.controller.press_import())
        # Create the button set it as disabled when the result is empty
        file_menu.add_command(label="Save as...", command=lambda: self.controller.press_save())
        file_menu.entryconfigure("Save as...", state='disabled')
        file_menu.add_separator()
        # Define the quit button
        file_menu.add_command(label="Quit", command=lambda: self.window.destroy())

        # Put the menu button to the menu bar
        menubar.add_cascade(label='File', menu=file_menu)
        self.window.config(menu=menubar)
        # Widget for the buttons of homepage
        programme_menu = Menu(menubar, tearoff=0)
        programme_menu.add_command(label="Burrows–Wheeler transform",
                                   command=lambda: self.BWT_page(self.window))
        programme_menu.add_command(label="Huffman coding",
                                   command=lambda: self.Huffman_page(self.window))
        menubar.add_cascade(label='Programme', menu=programme_menu)
        # Define the result display type of the menu bar
        mode_menu = Menu(menubar, tearoff=0)
        mode_menu.add_checkbutton(label="Show Result", variable=self.check_mode, state='disabled',
                                  onvalue=1, offvalue=1, command=lambda: self.check_mode.get())
        mode_menu.add_checkbutton(label="Show Step by step", variable=self.check_mode, state='disabled',
                                  onvalue=2, offvalue=1,  command=lambda: self.check_mode.get())
        menubar.add_cascade(label='Mode', menu=mode_menu)

        # Open the homepage by default
        self.Homepage(self.window)

    def Homepage(self, window):
        """
        Function to display the homepage
        """
        # Deleted the widget  if exists.
        try:
            self.Home_frame.destroy()
            self.BWT_title.destroy()
            self.Huff_title.destroy()
        except:
            pass
        # Reset the program variable and disabled the button
        self.open_window = ''
        self.controller.result = ''
        file_menu.entryconfigure("Import file from...", state='disabled')
        file_menu.entryconfigure("Save as...", state='disabled')

        # Set up different frame for disposition
        self.Home_frame = Frame(window)
        self.Home_frame.pack(fill='both',expand=1, pady=30)
        title_frame = Frame(self.Home_frame)
        title_frame.pack(pady=30)

        # Prepare the content to be displayed on the homepage
        intro = "This is the program to compress or decompress the data. " \
                "\nYou have two type of transformation program to choose." \
                "\nThe data computed will be converted into another series of " \
                "\ncharacters to reduce the original file size." \
                "\nAnd you can also compute the encoding file to get the original file."
        Label(title_frame, text='Welcome to the transformation tools',
              font=('Times', 25, 'bold', 'underline')).pack(pady=10)
        Label(title_frame, text='Written by Juejun CHEN', font=('Times', 13, 'bold')).pack(pady=10)
        Message(title_frame, text=intro, justify='center',
                width=800, font=('Helvetica', 13, 'normal')).pack(fill='both', pady=10)

        Label(self.Home_frame, text='Please choose the program:',
              font=('Times', 15, 'bold')).pack()

        prog_button = Frame(self.Home_frame)
        prog_button.pack(pady=30)
        Button(prog_button, text='Burrows–Wheeler transform', relief='raised',
               width=25, height=4, font=('Times', 15, 'bold'),
               command=lambda: self.BWT_page(self.window)).pack(side='left', padx=30, pady=20)
        Button(prog_button, text='Huffman coding',  relief='raised',
               width=25, height=4, font=('Times', 15, 'bold'),
               command=lambda: self.Huffman_page(self.window)).pack(side='left', padx=30, pady=20)

    def get_screen_size(self, window):
        """ Return the size of screen. """
        return window.winfo_screenwidth(), window.winfo_screenheight()

    def center_window(self, window, width, height):
        """
        Show the interface at the centre of screen.
        """
        screenwidth = window.winfo_screenwidth()
        screenheight = window.winfo_screenheight()
        size = '%dx%d+%d+%d' % (width, height, (screenwidth - width)/2, (screenheight - height)/2)
        window.geometry(size)

    def BWT_page(self, window):
        """ Generate and display to BWT transformation """
        # Deleted the widget  if exists.
        try:
            self.Home_frame.destroy()
            self.BWT_title.destroy()
            self.Huff_title.destroy()
            # Clean the transformation result when change the program
            self.controller.result = ''
        except:
            pass
        # Store the variable for the program
        self.open_window = "BWT"
        # Active the display type button when new widget is open
        self.active_button()
        # Define the title widget
        self.BWT_title = LabelFrame(window, text="Burrows–Wheeler transform",
                                    font=('Times', 20, 'bold'), padx=20, pady=20)
        self.BWT_title.pack(fill="both", expand=1)
        Label(self.BWT_title, text="Entry your sequence :", font=('Helvetica', 10, 'bold')).pack()
        Entry(self.BWT_title, textvariable=self.bwt_entry, width=60, bg='white').pack()

        # Create the program button for BWT widget
        self.create_checkbutton(self.BWT_title, self.bwt_entry)
        # Define the result widget
        self.BWT_result = LabelFrame(self.BWT_title, text="Result zone",
                                     font=('Times', 20, 'bold'), padx=20, pady=20)
        self.BWT_result.pack(fill="both", expand=1)

    def Huffman_page(self, window):
        """ Generate and display to Huffman coding transformation """
        # Deleted the widget  if exists.
        try:
            self.Home_frame.destroy()
            self.BWT_title.destroy()
            self.Huff_title.destroy()
            # Clean the transformation result when change the program
            self.controller.result = ''
        except:
            pass
        # Store the variable for the program
        self.open_window = "HUFF"
        # Active the display type button when new widget is open
        self.active_button()
        # Define the title widget
        self.Huff_title = LabelFrame(window, text="Huffman coding",
                                     font=('Times', 20, 'bold'),
                                     padx=20, pady=20)
        self.Huff_title.pack(fill="both", expand=1)
        Label(self.Huff_title, text="Entry your sequence :", font=('Helvetica', 10, 'bold')).pack()
        Entry(self.Huff_title, textvariable=self.huff_entry, width=60, bg='white').pack()
        # Create the program button for Huffman coding widget
        self.create_checkbutton(self.Huff_title, self.huff_entry, self.code_entry)
        # Define the result widget
        self.Huff_result = LabelFrame(self.Huff_title, text="Result zone",
                                      font=('Times', 20, 'bold'), padx=20, pady=20)
        self.Huff_result.pack(fill="both", expand=1)

    def create_checkbutton(self, algo_name, entry, dico=None):
        """
        Create the button region to choose the type of program and sequence.
        """
        # Create the children widgets for different transformation
        sequence_type = Frame(algo_name)
        sequence_type.pack()
        # Create the frame for the type of sequence
        self.type_content = Frame(sequence_type)
        self.type_content.pack()
        type_label = Label(self.type_content, text="The type of sequence is :")
        type_label.pack(side='left')
        # Create 3 buttons related to the same variable, the choice will be unique.
        r1 = Radiobutton(self.type_content, text="DNA sequence", variable=self.s, value=1,
                         command=lambda: self.check_sequence())
        r1.pack(side='left')
        r2 = Radiobutton(self.type_content, text="RNA sequence", variable=self.s, value=2,
                         command=lambda: self.check_sequence())
        r2.pack(side='left')
        r3 = Radiobutton(self.type_content, text="Others", variable=self.s, value=3,
                         command=lambda: self.check_sequence())
        r3.pack(side='left')

        # Create the frame for the type of program (encode or decode)
        program_zone = Frame(algo_name)
        program_zone.pack()
        Label(program_zone, text="Program :").pack(side='left')
        # Create 2 buttons related to the same variable, the choice will be unique.
        r4 = Radiobutton(program_zone, text="Encode", variable=self.p, value=1, command=lambda: self.check_sequence())
        r4.pack(side='left')
        r5 = Radiobutton(program_zone, text="Decode", variable=self.p, value=2, command=lambda: self.check_sequence())
        r5.pack(side='left')

        # Create the widget in case of the Huffman decoding is chosen
        try:
            self.dico_zone = Frame(self.Huff_title)
            self.dico_zone.pack()
            # Create the label and entry widget but hide these widgets for now
            self.dico_label = Label(self.dico_zone, text='Please enter your decode dictionary :',
                                    font=('Helvetica', 10, 'bold'))
            self.dico_label.pack_forget()
            self.code_dict = Entry(self.dico_zone, textvariable=self.code_entry, width=60, bg='white')
            self.code_dict.pack_forget()
        except:
            pass
        # Create the button zone
        button_zone = Frame(algo_name)
        button_zone.pack()
        # Create different reset plan for the chosen program
        if self.open_window == "BWT":
            Button(button_zone, text='Compute',
                   command=(lambda button=self.open_window:
                            self.controller.press_button_program(button))).pack(side='left', padx=30)
            Button(button_zone, text='Refresh', command=lambda: entry.set('')).pack(side='left', padx=30)
        elif self.open_window == "HUFF":
            Button(button_zone, text='Compute',
                   command=(lambda button=self.open_window:
                            self.controller.press_button_program(button))).pack(side='left', padx=30)
            Button(button_zone, text='Refresh',
                   command=lambda: (entry.set(''), dico.set(''))).pack(side='left', padx=30)

    def check_sequence(self):
        # Encode program is chosen
        if self.p.get() == 1:
            # Show the button used for encode and hide the unnecessary button
            try:
                self.type_content.pack()
                self.code_entry.set('')
                self.dico_label.pack_forget()
                self.code_dict.pack_forget()
            except:
                pass
            # Show different message on the terminal
            if self.s.get() == 1:
                # Compute DNA sequence
                self.prog = 11
            elif self.s.get() == 2:
                # Compute RNA sequence
                self.prog = 21
            elif self.s.get() == 3:
                # Compute other sequence
                self.prog = 31
        # Decode program is chosen.
        elif self.p.get() == 2:
            try:
                # Show the button used for decode and hide the unnecessary button
                self.type_content.pack_forget()
                self.code_entry.set('')
                # Show dictionary widget when Huffman coding is chosen.
                if self.open_window == "HUFF":
                    self.dico_label.pack()
                    self.code_dict.pack()
            except:
                pass
            # Show different message on the terminal
            if self.s.get() == 1:
                # Compute DNA sequence
                self.prog = 12
            elif self.s.get() == 2:
                # Compute RNA sequence
                self.prog = 22
            elif self.s.get() == 3:
                # Compute other sequence
                self.prog = 32

    def get_value(self, x, dico=None):
        """ Get the input """
        y = x.get()
        return y

    def is_DNA(self, sequence):
        # Create a nucleotide list to compare with input sequence
        DNA_nucleotide = ['A', 'C', 'G', 'T', 'N', '$']
        sequence = sequence.upper().replace(' ', '').replace('\n', '')
        # Return False if the character is not in list
        for letter in sequence:
            if letter not in DNA_nucleotide:
                return False
        return True

    def is_RNA(self, sequence):
        # Create a nucleotide list to compare with input sequence
        RNA_nucleotide = ['A', 'C', 'G', 'U', 'N', '$']
        sequence = sequence.upper().replace(' ', '').replace('\n', '')
        # Return False if the character is not in list
        for letter in sequence:
            if letter not in RNA_nucleotide:
                return False
        return True

    def show_error(self):
        """ Show error message and reset the entry widget """
        messagebox.showerror('Error', "The sequence is invalid, please re-enter.")
        if self.open_window == "BWT":
            self.bwt_entry.set('')
        elif self.open_window == "HUFF":
            self.huff_entry.set('')
            if self.code_entry.get() != '':
                self.code_entry.set('')
        return False

    def show_info(self):
        """ Show the finished info when program is finished"""
        messagebox.showinfo('Info', 'It\'s finished!!!')
        return False

    def active_button(self):
        """ Use to activate the corresponding button when necessary """
        file_menu.entryconfigure("Import file from...", state='active')
        file_menu.entryconfigure("Save as...", state='disabled')
        # Activate the save button when the result is not empty
        if self.controller.result != '':
            file_menu.entryconfigure("Save as...", state='active')
        # Activate the display mode button when the program is chosen
        elif self.open_window == "BWT" or "HUFF":
            mode_menu.entryconfigure("Show Result", state='active')
            mode_menu.entryconfigure("Show Step by step", state='active')

    def show_result(self, frame_name):
        """ Return the transformation result after computing."""
        try:
            # Destroy the widget if exist
            self.result_zone.destroy()
            self.step_button.destroy()
        except:
            pass
        # Create the frame to show result
        self.result_zone = Frame(frame_name)
        self.result_zone.pack(fill='both', expand=1)
        # Create the display mode button but hide button when unnecessary
        self.step_button = Frame(frame_name)
        self.step_button.pack_forget()
        Button(self.step_button, text='Next', command=lambda: self.controller.press_next()).pack(side='left', padx=30)
        Button(self.step_button, text='Show all',
               command=lambda: self.controller.press_show_all()).pack(side='left', padx=30)
        # Create a text widget to show result
        self.encode_result = scrolledtext.ScrolledText(self.result_zone, height=17, bg='white')
        self.encode_result.pack(fill='x', expand=1)
        # When the BWT program is chosen
        if self.open_window == "BWT":
            self.show_bwt_result()
        # When the Huffman program is chosen
        elif self.open_window == "HUFF":
            self.show_huff_result()
        # Always show the last line of result
        self.encode_result.see("end")
        self.active_button()

    def show_bwt_result(self):
        """ Show different display mode for BWT transformation when necessary"""
        # When display mode is 'Show result'
        if self.check_mode.get() == 1:
            # Insert result to the text result zone
            self.encode_result.insert("end", '%s' % self.controller.result)
            # The content of text result zone can not be modified outside the program
            self.encode_result.configure(state='disabled')
        # When display mode is 'Show step by step'
        elif self.check_mode.get() == 2:
            # Show the display button
            self.step_button.pack()
            if self.controller.press_button == 'next':
                # Insert each step of transformation when click 'next' button
                self.encode_result.insert("end",  '%s' % self.controller.show_step)
                # The content of text result zone can not be modified outside the program
                self.encode_result.configure(state='disabled')
            elif self.controller.press_button == 'show all':
                # Insert all steps of transformation when click 'show all' button
                self.encode_result.insert("end", '%s' % self.controller.show_all)
                # The content of text result zone can not be modified outside the program
                self.encode_result.configure(state='disabled')

    def show_huff_result(self):
        """ Show different display mode for Huffman coding transformation when necessary"""
        # When display mode is 'Show result'
        if self.check_mode.get() == 1:
            if self.p.get() == 1:
                # Print the binary dictionary
                self.encode_result.insert("end", '%s' % self.controller.code_dict + '\n')
            self.encode_result.insert("end", '%s' % self.controller.result)
            self.encode_result.configure(state='disabled')
        # When display mode is 'Show step by step'
        elif self.check_mode.get() == 2:
            # Show the display button
            self.step_button.pack()
            if self.controller.press_button == 'next':
                # Insert each step of transformation when click 'next' button
                self.encode_result.insert("end", '%s' % self.controller.show_step)
                self.encode_result.configure(state='disabled')
            elif self.controller.press_button == 'show all':
                # Insert all steps of transformation when click 'show all' button
                self.encode_result.insert("end", '%s' % self.controller.show_all)
                self.encode_result.configure(state='disabled')

    def save_as(self):
        # Create a new file to save the result.
        self.file_name = filedialog.asksaveasfile(mode='w', defaultextension=".txt")

    def import_file(self):
        # Open an exist file to compute.
        self.file_name = filedialog.askopenfilename(title='Please choose your file',
                                                    filetypes=[('', '*.txt')])

    def show_success(self):
        # Show a message when data is imported
        messagebox.showinfo('Info', 'The file has been imported successfully!')

    def main(self):
        """ Function to generate all the interface """
        # Create a title for the interface
        self.window.title("Transformation Tools")
        # Show interface at the center of screen and define the size of interface
        self.center_window(self.window, 900, 650)
        self.window.configure()
        self.window.mainloop()
